package app;

import java.util.Random;


import core.Pantalla;
import core.Player;
import core.Profession;
import core.Teclado;

public class Main {

	public static void main(String[] args) {
		
		Pantalla.pideNombreJugador();
		String nombreJugador = Teclado.getNombreJugador();
	
		int action = 0;
		Player jugador = new Player(nombreJugador);
		
		
		
		jugador.getPlayer().load();
		
		while(true) {
			Pantalla.print(jugador);
			Pantalla.printMenu(jugador);
			action = Teclado.getOptionMenu(jugador);
			if(action == 1) {
				Pantalla.trabaja(jugador);
			}else if(action == 2) {
				Pantalla.compra(jugador);
			}else if(action == 3) {
				Pantalla.vende(jugador);
			}else if(action == 4) {
				Pantalla.aprenderProfesion();
				
			}else if (action == 5) {
				jugador.getPlayer().save();
				break;
			}
			
			
			jugador.sleep();
		}
		
		Pantalla.resumen(jugador);
	}
}
